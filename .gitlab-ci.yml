stages:
  - test
  - reporting
  - coverage
  - documentation
  - pages

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: '$CI_COMMIT_BRANCH =~ /^(main|releases\/)/'

default:
  image: python:3.10

cache:
  paths:
    - .cache/pip

.install_dependencies:
  before_script:
      - python --version
      - pip install --upgrade pip
      - pip install tox

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PY_VERSION: "3.10"

test:
  image: python:$PY_VERSION
  extends: .install_dependencies
  stage: test
  artifacts:
    reports:
      junit: test_report.xml
  script:
    # TODO: simplify again and turn 3.10.4 -> 3.10 (once the mypy error with numpy is fixed)
    - env=${PY_VERSION//./}
    - tox -e py${env:0:3}
  parallel:
    matrix:
      - PY_VERSION: ["3.8", "3.9", "3.10.4"]

file_mode_junit_report:
  image: python:3.10
  extends: .install_dependencies
  stage: reporting
  script:
    - python -m pip install -r requirements.txt
    - python -m pip install .
    - fieldcompare file test/data/test_mesh.vtu -r test/data/test_mesh.vtu --exclude-fields "function" --junit-xml file_mode_junit.xml
  artifacts:
    reports:
      junit: file_mode_junit.xml

dir_mode_junit_report:
  image: python:3.10
  extends: .install_dependencies
  stage: reporting
  script:
    - python -m pip install -r requirements.txt
    - python -m pip install .
    - fieldcompare dir test/data/ -r test/data/ --include-files "*.vtu" --junit-xml dir_mode_junit.xml
  artifacts:
    reports:
      junit: dir_mode_junit.xml

test_coverage_report:
  stage: coverage
  extends: .install_dependencies
  script:
    - tox -e coverage
  coverage: '/TOTAL.*\s([.\d]+)%/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - cov_report
    expire_in: 30 minutes

build_docs:
  stage: documentation
  extends: .install_dependencies
  script:
    - pip install sphinx sphinx-mdinclude pydata-sphinx-theme -r requirements.txt
    - cd docs
    - sphinx-apidoc -f -o build ../
    - make html
  artifacts:
    paths:
      - docs/build
    expire_in: 30 minutes

pages:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: pages
  needs:
    - job: test_coverage_report
      artifacts: true
    - job: build_docs
      artifacts: true
  script:
    - mkdir -p public/coverage
    - mv cov_report/* public/coverage/
    - mv docs/build/html/* public/
  artifacts:
    paths:
      - public
    expire_in: 30 minutes
