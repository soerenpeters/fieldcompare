"""
fieldcompare provides functionality to compare field data, where field data are collections of
fields (each consisting of a name and an associated array of values) defined on domains. An example
would be discrete numerical solutions (fields) defined on a computational mesh (domain). This top-level
module exposes central classes in this context, mostly operating on the protocols defined in the "protocols"
module. Implementations of these protocols can be found in the submodules.
"""

from .__about__ import __version__

from ._field_sequence import FieldDataSequence
from ._field_data_comparison import (
    FieldDataComparator,
    FieldComparisonSuite,
    FieldComparison,
    FieldComparisonStatus
)

__all__ = [
    "FieldDataComparator",
    "FieldComparisonSuite",
    "FieldComparison",
    "FieldComparisonStatus",
    "__version__"
]
