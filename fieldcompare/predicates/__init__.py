"""Predefined predicate classes for comparing field values."""

from ._predicates import (
    ExactEquality,
    FuzzyEquality,
    DefaultEquality,
    PredicateResult,
    PredicateError
)

__all__ = [
    "ExactEquality",
    "FuzzyEquality",
    "DefaultEquality",
    "PredicateResult",
    "PredicateError"
]
