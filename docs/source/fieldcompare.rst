fieldcompare
------------

.. automodule:: fieldcompare
   :members:
   :undoc-members:
   :show-inheritance:


fieldcompare.protocols
----------------------

.. automodule:: fieldcompare.protocols
   :members:
   :undoc-members:
   :exclude-members: __init__
   :show-inheritance:
